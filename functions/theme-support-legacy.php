<?php
/**
 * Functions and definitions
 *
 * Opinionated set of theme functions with necessary features and settings.
 * All features should be declared within `after_setup_theme` action hook.
 *
 * Also the `content_width` global is part of a theme setup.
 *
 * Most of these examples are based on the Twenty Twenty One default WordPress
 * theme and adjusted according to WordPress Codex and Block Editor Handbook.
 *
 * NOTE: When working with a child theme setup its required support features
 * may differ much from a parent theme settings it is based on. In such case it
 * is better to redeclare parent theme setup function and thus wipe its
 * features begining with defaults and build your own set of features.
 * See a `child-theme-load-textdomain.php` file as an example.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 * @link https://developer.wordpress.org/block-editor/how-to-guides/themes/theme-support/
 * @link https://developer.wordpress.org/reference/functions/add_theme_support/
 * @link https://github.com/WordPress/twentytwentyone/blob/trunk/functions.php
 *
 * @package STWP
 */

if ( ! function_exists( 'stwp_theme_setup' ) ) {
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * @return void
	 */
	function stwp_theme_setup() {
		// Make theme available for translation.
		// Translations can be placed in the `/languages` directory.
		load_theme_textdomain( 'stwp', get_template_directory() . '/languages' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 1920, 9999 );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		$logo_width  = 1024;
		$logo_height = 128;

		add_theme_support(
			'custom-logo',
			array(
				'height'               => $logo_height,
				'width'                => $logo_width,
				'flex-width'           => true,
				'flex-height'          => true,
				'unlink-homepage-logo' => false,
			)
		);

		/**
		 * Register navigation menus.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/navigation-menus/
		 */
		register_nav_menus(
			array(
				'primary' => esc_html__( 'Primary menu', 'stwp' ),
				'footer'  => esc_html__( 'Footer menu', 'stwp' ),
			)
		);

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		// Let WordPress manage the document title.
		add_theme_support( 'title-tag' );

		// Add post-formats support.
		add_theme_support(
			'post-formats',
			array(
				'link',
				'aside',
				'gallery',
				'image',
				'quote',
				'status',
				'video',
				'audio',
				'chat',
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
				'navigation-widgets',
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		// Custom background color.
		add_theme_support(
			'custom-background',
			array(
				'default-color' => 'ffffff',
				'default-image' => '',
			)
		);

	}
	add_action( 'after_setup_theme', 'stwp_theme_setup' );
}

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 * Set priority to >10 for child theme to override parent theme variable.
 *
 * @global int $content_width
 */
function stwp_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'stwp_content_width', 750 );
}
add_action( 'after_setup_theme', 'stwp_content_width', 0 );
