<?php
/**
 * Functions and definitions
 *
 * Methods to add a child theme textdomain. All of them should be called within
 * an `after_setup_theme` action hook in a `functions.php` file. Theme setup
 * functions are used in these examples.
 *
 * The `load_child_theme_textdomain()` function is a wrapper function for the
 * `load_theme_textdomain()`. If you provide path to the directory it does not
 * really matter which one to use.
 *
 * Sometimes it is necessary to override some parent theme's translations and
 * you should do it locally in a child theme directory where they will not be
 * ovewritten on updates. See first example where it is done within a function
 * that overrides parent's theme function of the same name. It may be not what
 * you want to do. Call that function in a child theme's setup action hook then.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 * @link https://developer.wordpress.org/themes/advanced-topics/child-themes/
 *
 * @package STWP
 */

/**
 * Resets parent theme features which allows to redeclare them in a child theme.
 *
 * @return void
 */
function stwp_parent_theme_setup() {
	// Overrides parent theme strings in a child theme directory if neccessary.
	// This is only for strings with a parent theme textdomain.
	load_theme_textdomain( 'parent-theme', get_stylesheet_directory() . '/languages/parent-theme' );
}
add_action( 'after_setup_theme', 'stwp_parent_theme_setup' );

/**
 * Sets up child theme defaults and registers support for various WordPress features.
 * It recreates or overrides parent theme defaults.
 *
 * @return void
 */
function stwp_child_theme_setup() {
	// Make theme ready for translation.
	load_theme_textdomain( 'child-theme', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'stwp_child_theme_setup' );
