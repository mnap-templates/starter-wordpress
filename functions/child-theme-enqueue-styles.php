<?php
/**
 * Functions and definitions
 *
 * This function enqueues child theme stylesheets and should be placed in the
 * `functions.php` file of a theme or the main file of a plugin.
 *
 * It checks if the current browser is IE11 and loads stylesheet prepared for
 * the legacy browser. Remove if not necessary (WordPress 5.8 dropped support
 * for the IE11).
 * It checks if the current environment is development or production environment
 * (simple check if WP_DEBUG is set to true) and loads minified stylesheets for
 * the latter.
 * Adds enqueue for RTL versions of stylesheets.
 * Adds separate stylesheets for printing.
 *
 * Two types of versioning are available: volatile (using filemtime()) which is
 * suitable for a development and theme version for a production environment.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 * @link https://codex.wordpress.org/Right-to-Left_Language_Support
 *
 * @package STWP
 */

/**
 * Enqueue child theme styles.
 *
 * @return void
 */
function stwp_enqueue_styles() {
	// Note, the is_IE global variable is defined by WordPress and is used
	// to detect if the current browser is internet explorer.
	global $is_IE;

	if ( defined( 'WP_DEBUG' ) && true === WP_DEBUG ) {
		$min = '';
	} else {
		$min = '.min';
	}

	$child_style       = 'child-theme-style';
	$child_print_style = 'child-theme-print-style';
	// TODO: remove this comment in a production.
	// $child_version     = wp_get_theme()->get( 'Version' ); // Only works if you have Version in the style header.
	$child_version = filemtime( get_stylesheet_directory() . '/style.css' ); // Volatile versioning for a development.

	if ( $is_IE ) {
		// If IE 11 or below, use a flattened stylesheet with static values replacing CSS Variables.
		wp_enqueue_style(
			$child_style,
			get_stylesheet_directory_uri() . '/assets/css/ie' . $min . '.css',
			array(),
			$child_version,
			'all'
		);
	} else {
		// If not IE, use the standard stylesheet.
		wp_enqueue_style(
			$child_style,
			get_stylesheet_directory_uri() . '/style' . $min . '.css',
			array(),
			$child_version,
			'all'
		);
	}

	// RTL styles.
	wp_style_add_data( $child_style, 'rtl', 'replace' );
	wp_style_add_data( $child_style, 'suffix', $min );

	// Print styles.
	wp_enqueue_style(
		$child_print_style,
		get_stylesheet_directory_uri() . '/assets/css/print' . $min . '.css',
		array(),
		$child_version,
		'print'
	);
}
add_action( 'wp_enqueue_scripts', 'stwp_enqueue_styles' );
