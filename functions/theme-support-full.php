<?php
/**
 * Functions and definitions
 *
 * Opinionated set of theme functions with necessary features and settings.
 * All features should be declared within `after_setup_theme` action hook.
 *
 * Also the `content_width` global is part of a theme setup.
 *
 * Most of these examples are based on the Twenty Twenty One default WordPress
 * theme and adjusted according to WordPress Codex and Block Editor Handbook.
 *
 * NOTE1: When working with a child theme setup its required support features
 * may differ much from a parent theme settings it is based on. In such case it
 * is better to redeclare parent theme setup function and thus wipe its
 * features begining with defaults and build your own set of features.
 * See a `child-theme-load-textdomain.php` file as an example.
 *
 * NOTE2: First introduced in WordPress 5.0.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 * @link https://developer.wordpress.org/block-editor/how-to-guides/themes/theme-support/
 * @link https://developer.wordpress.org/reference/functions/add_theme_support/
 * @link https://github.com/WordPress/twentytwentyone/blob/trunk/functions.php
 *
 * NOTE3: The `theme.json` settings will take precedence over the values declared
 * via `add_theme_support`.
 *
 * @link https://developer.wordpress.org/block-editor/how-to-guides/themes/theme-json/
 *
 * @package STWP
 */

/**
 * Resets parent theme features which allows to redeclare them in a child theme.
 *
 * @return void
 */
function stwp_parent_theme_setup() {
	/**
	 * Overrides parent theme strings in a child theme directory if neccessary.
	 * This is only for strings with a parent theme textdomain.
	 */
	load_theme_textdomain( 'stwp_parent_theme', get_stylesheet_directory() . '/languages/stwp_parent_theme' );
}
add_action( 'after_setup_theme', 'stwp_parent_theme_setup' );

if ( ! function_exists( 'stwp_theme_setup' ) ) {
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * @return void
	 */
	function stwp_theme_setup() {
		// Make theme available for translation.
		// Translations can be placed in the `/languages` directory.
		load_theme_textdomain( 'stwp', get_template_directory() . '/languages' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 1920, 9999 );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		$logo_width  = 1024;
		$logo_height = 128;

		add_theme_support(
			'custom-logo',
			array(
				'height'               => $logo_height,
				'width'                => $logo_width,
				'flex-width'           => true,
				'flex-height'          => true,
				'unlink-homepage-logo' => false,
			)
		);

		/**
		 * Register navigation menus.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/navigation-menus/
		 */
		register_nav_menus(
			array(
				'primary' => esc_html__( 'Primary menu', 'stwp' ),
				'footer'  => esc_html__( 'Footer menu', 'stwp' ),
			)
		);

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		// Let WordPress manage the document title.
		add_theme_support( 'title-tag' );

		// Add post-formats support.
		add_theme_support(
			'post-formats',
			array(
				'link',
				'aside',
				'gallery',
				'image',
				'quote',
				'status',
				'video',
				'audio',
				'chat',
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
				'navigation-widgets',
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		// Add support for full and wide align images.
		add_theme_support( 'align-wide' );

		// Add support for responsive embedded content.
		add_theme_support( 'responsive-embeds' );

		// Add support for custom line height controls.
		add_theme_support( 'custom-line-height' );

		// Add support for experimental link color control.
		// Can be removed when the theme requires WordPress 5.8 as the minimum version.
		add_theme_support( 'experimental-link-color' );

		// Add support for experimental cover block spacing.
		add_theme_support( 'custom-spacing' );

		// Add support for custom units.
		// This was removed in WordPress 5.6 but is still required to properly support WP 5.5.
		add_theme_support( 'custom-units' );

		// Add support for Block Styles.
		// add_theme_support( 'wp-block-styles' ); // WARNING: opinionated styles, might override and break some styles, i.e. horizontal rule.

		// Add support for editor styles.
		add_theme_support( 'editor-styles' );
		add_theme_support( 'dark-editor-style' );

		// Enqueue editor styles.
		$editor_stylesheet_path = './assets/css/style-editor.css';
		add_editor_style( $editor_stylesheet_path );

		// Add custom editor font sizes.
		$xs = 12;
		$sm = 14;
		$md = 16;
		$lg = 32;
		$xl = 40;
		add_theme_support(
			'editor-font-sizes',
			array(
				array(
					'name'      => esc_html__( 'Extra small', 'stwp' ),
					'shortName' => esc_html_x( 'XS', 'Font size', 'stwp' ),
					'size'      => $xs,
					'slug'      => 'extra-small',
				),
				array(
					'name'      => esc_html__( 'Small', 'stwp' ),
					'shortName' => esc_html_x( 'S', 'Font size', 'stwp' ),
					'size'      => $sm,
					'slug'      => 'small',
				),
				array(
					'name'      => esc_html__( 'Normal', 'stwp' ),
					'shortName' => esc_html_x( 'M', 'Font size', 'stwp' ),
					'size'      => $md,
					'slug'      => 'normal',
				),
				array(
					'name'      => esc_html__( 'Large', 'stwp' ),
					'shortName' => esc_html_x( 'L', 'Font size', 'stwp' ),
					'size'      => $lg,
					'slug'      => 'large',
				),
				array(
					'name'      => esc_html__( 'Extra large', 'stwp' ),
					'shortName' => esc_html_x( 'XL', 'Font size', 'stwp' ),
					'size'      => $xl,
					'slug'      => 'extra-large',
				),
			)
		);

		// Custom background color.
		add_theme_support(
			'custom-background',
			array(
				'default-color' => 'ffffff',
				'default-image' => '',
			)
		);

		// Editor color palette.
		$black     = '#282828';
		$dark_gray = '#474747';
		$gray      = '#cdcdcd';
		$green     = '#1d9700';
		$red       = '#d6000c';
		$yellow    = '#c49700';
		$white     = '#ffffff';

		add_theme_support(
			'editor-color-palette',
			array(
				array(
					'name'  => esc_html__( 'Black', 'stwp' ),
					'slug'  => 'black',
					'color' => $black,
				),
				array(
					'name'  => esc_html__( 'Dark gray', 'stwp' ),
					'slug'  => 'dark-gray',
					'color' => $dark_gray,
				),
				array(
					'name'  => esc_html__( 'Gray', 'stwp' ),
					'slug'  => 'gray',
					'color' => $gray,
				),
				array(
					'name'  => esc_html__( 'Green', 'stwp' ),
					'slug'  => 'green',
					'color' => $green,
				),
				array(
					'name'  => esc_html__( 'Red', 'stwp' ),
					'slug'  => 'red',
					'color' => $red,
				),
				array(
					'name'  => esc_html__( 'Yellow', 'stwp' ),
					'slug'  => 'yellow',
					'color' => $yellow,
				),
				array(
					'name'  => esc_html__( 'White', 'stwp' ),
					'slug'  => 'white',
					'color' => $white,
				),
			)
		);

		add_theme_support(
			'editor-gradient-presets',
			array(
				array(
					'name'     => esc_html__( 'Dark gray to gray', 'stwp' ),
					'gradient' => 'linear-gradient(60deg, ' . $dark_gray . ' 0%, ' . $gray . ' 100%)',
					'slug'     => 'dark-gray-to-gray',
				),
			)
		);
	}
	add_action( 'after_setup_theme', 'stwp_theme_setup' );
}

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 * Set priority to >10 for child theme to override parent theme variable.
 *
 * @global int $content_width
 */
function stwp_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'stwp_content_width', 750 );
}
add_action( 'after_setup_theme', 'stwp_content_width', 0 );
