<?php
/**
 * Functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 * @link https://developer.wordpress.org/block-editor/how-to-guides/themes/theme-support/
 * @link https://developer.wordpress.org/reference/functions/add_theme_support/
 * @link https://github.com/WordPress/twentytwentyone/blob/trunk/functions.php
 *
 * @package STWP
 */

/**
 * Filter the maximum number of words in a post excerpt.
 *
 * @param int $length The maximum number of words.
 */
function stwp_custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'stwp_custom_excerpt_length', 999 );
