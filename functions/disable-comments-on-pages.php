<?php
/**
 * Functions and definitions
 *
 * This function disables comments on pages.
 * Use it if your website uses pages as a static content and most of
 * the discussion with users takes place in a blog.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package STWP
 */

/**
 * Disable comments on pages.
 *
 * @param bool $open    Whether comments should be open.
 * @param int  $post_id Post ID.
 * @return bool Whether comments should be open.
 */
function stwp_disable_pages_comments( $open, $post_id ) {
	$post = get_post( $post_id );
	if ( 'page' === $post->post_type ) {
		$open = false;
	}
	return $open;
}
add_filter( 'comments_open', 'stwp_disable_pages_comments', 10, 2 );
