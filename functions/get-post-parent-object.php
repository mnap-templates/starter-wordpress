<?php
/**
 * Determines if a post has a paternt and gets the related parent Post object.
 *
 * @link https://make.wordpress.org/core/2021/02/10/introducing-new-post-parent-related-functions-in-wordpress-5-7/
 * @package STWP
 */

/**
 * Example "Back to parent page" link.
 */
function stwp_parent_page_link() {
	?>
	<?php if ( has_post_parent() ) : ?>
		<a href="<?php the_permalink( get_post_parent() ); ?>">
		<?php
			echo esc_html(
				sprintf(
					// translators: page link.
					__( 'Back to parent page: %s', 'stwp' ),
					get_the_title( get_post_parent() )
				)
			);
		?>
		</a>
	<?php endif; ?>
	<?php
};
