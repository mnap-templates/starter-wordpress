<?php
/**
 * Functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 * @link https://developer.wordpress.org/block-editor/how-to-guides/themes/theme-support/
 * @link https://developer.wordpress.org/reference/functions/add_theme_support/
 * @link https://github.com/WordPress/twentytwentyone/blob/trunk/functions.php
 *
 * @package STWP
 */

/**
 * Remove the archive title prefix.
 */
add_filter( 'get_the_archive_title_prefix', '__return_false' );
