<?php
/**
 * In WordPress 5.7, a new dynamic filter, render_block_{$this->name}, was
 * introduced, allowing a function to be hooked only to blocks of a specific type.
 * For example, the paragraph block, registered with a name of core/paragraph
 * can be filtered using render_block_core/paragraph.
 *
 * @see https://make.wordpress.org/core/2021/02/18/wordpress-5-7-a-new-dynamic-hook-to-filter-the-content-of-a-single-block/
 * @package STWP
 */

/**
 * Wrap all core/paragragh blocks in a <div>.
 *
 * @param string $block_content The block content about to be appended.
 * @param array  $block         The full block, including name and attributes.
 * @return string Modified block content.
 */
function stwp_paragraph_block_wrapper( $block_content, $block ) {
	$content = '<div class="my-custom-wrapper">' . $block_content . '</div>';
	return $content;
}
add_filter( 'render_block_core/paragraph', 'stwp_paragraph_block_wrapper', 10, 2 );

/**
 * Wrap all core/paragraph blocks with large font size in a <div>.
 *
 * @param string $block_content The block content about to be appended.
 * @param array  $block         The full block, including name and attributes.
 * @return string Modified block content.
 */
function stwp_large_sized_paragraph_block_wrapper( $block_content, $block ) {
	if ( isset( $block['attrs']['fontSize'] ) && 'large' === $block['attrs']['fontSize'] ) {
		$block_content = '<div class="my-large-paragraph">' . $block_content . '</div>';
	}
	return $block_content;
}
add_filter( 'render_block_core/paragraph', 'stwp_large_sized_paragraph_block_wrapper', 10, 2 );
