<?php
/**
 * Functions and definitions
 *
 * Import files from `includes`/`inc` directory that hold additional theme tag
 * templates and functionalities not related to the main theme's features and
 * settings.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package STWP
 */

// Import parent theme file.
require_once get_template_directory() . 'inc/template-tags';

// Import child theme file.
require_once get_stylesheet_directory() . 'inc/template-tags';
