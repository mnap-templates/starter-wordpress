<?php
/**
 * Functions and definitions
 *
 * The Blocks enhacements to opt-in to, extensions and customization.
 * It should be bundled in a one action hook and function with base theme
 * support features.
 *
 * Most of these examples are based on the Twenty Twenty One default WordPress
 * theme and adjusted according to WordPress Codex and Block Editor Handbook.
 *
 * NOTE: First introduced in WordPress 5.0.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 * @link https://developer.wordpress.org/block-editor/how-to-guides/themes/theme-support/
 * @link https://developer.wordpress.org/reference/functions/add_theme_support/
 * @link https://github.com/WordPress/twentytwentyone/blob/trunk/functions.php
 *
 * NOTE2: The `theme.json` settings will take precedence over the values declared
 * via `add_theme_support`.
 *
 * @link https://developer.wordpress.org/block-editor/how-to-guides/themes/theme-json/
 *
 * @package STWP
 */

if ( ! function_exists( 'stwp_theme_setup' ) ) {
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * @return void
	 */
	function stwp_theme_setup() {
		// Add support for Block Styles.
		// add_theme_support( 'wp-block-styles' ); // WARNING: opinionated styles, might override and break some styles, i.e. horizontal rule.

		// Add support for full and wide align images.
		add_theme_support( 'align-wide' );

		// Add support for responsive embedded content.
		add_theme_support( 'responsive-embeds' );

		// Add support for custom line height controls.
		add_theme_support( 'custom-line-height' );

		// Add support for experimental link color control.
		// Can be removed when the theme requires WordPress 5.8 as the minimum version.
		add_theme_support( 'experimental-link-color' );

		// Add support for experimental cover block spacing.
		add_theme_support( 'custom-spacing' );

		// Add support for custom units.
		// This was removed in WordPress 5.6 but is still required to properly support WP 5.5.
		add_theme_support( 'custom-units' );

		// Add support for editor styles.
		add_theme_support( 'editor-styles' );
		add_theme_support( 'dark-editor-style' );

		// Enqueue editor styles.
		$editor_stylesheet_path = './assets/css/style-editor.css';
		add_editor_style( $editor_stylesheet_path );

		// Add custom editor font sizes.
		$xs = 12;
		$sm = 14;
		$md = 16;
		$lg = 32;
		$xl = 40;
		add_theme_support(
			'editor-font-sizes',
			array(
				array(
					'name'      => esc_html__( 'Extra small', 'stwp' ),
					'shortName' => esc_html_x( 'XS', 'Font size', 'stwp' ),
					'size'      => $xs,
					'slug'      => 'extra-small',
				),
				array(
					'name'      => esc_html__( 'Small', 'stwp' ),
					'shortName' => esc_html_x( 'S', 'Font size', 'stwp' ),
					'size'      => $sm,
					'slug'      => 'small',
				),
				array(
					'name'      => esc_html__( 'Normal', 'stwp' ),
					'shortName' => esc_html_x( 'M', 'Font size', 'stwp' ),
					'size'      => $md,
					'slug'      => 'normal',
				),
				array(
					'name'      => esc_html__( 'Large', 'stwp' ),
					'shortName' => esc_html_x( 'L', 'Font size', 'stwp' ),
					'size'      => $lg,
					'slug'      => 'large',
				),
				array(
					'name'      => esc_html__( 'Extra large', 'stwp' ),
					'shortName' => esc_html_x( 'XL', 'Font size', 'stwp' ),
					'size'      => $xl,
					'slug'      => 'extra-large',
				),
			)
		);

		// Editor color palette.
		$black     = '#282828';
		$dark_gray = '#474747';
		$gray      = '#cdcdcd';
		$green     = '#1d9700';
		$red       = '#d6000c';
		$yellow    = '#c49700';
		$white     = '#ffffff';

		add_theme_support(
			'editor-color-palette',
			array(
				array(
					'name'  => esc_html__( 'Black', 'stwp' ),
					'slug'  => 'black',
					'color' => $black,
				),
				array(
					'name'  => esc_html__( 'Dark gray', 'stwp' ),
					'slug'  => 'dark-gray',
					'color' => $dark_gray,
				),
				array(
					'name'  => esc_html__( 'Gray', 'stwp' ),
					'slug'  => 'gray',
					'color' => $gray,
				),
				array(
					'name'  => esc_html__( 'Green', 'stwp' ),
					'slug'  => 'green',
					'color' => $green,
				),
				array(
					'name'  => esc_html__( 'Red', 'stwp' ),
					'slug'  => 'red',
					'color' => $red,
				),
				array(
					'name'  => esc_html__( 'Yellow', 'stwp' ),
					'slug'  => 'yellow',
					'color' => $yellow,
				),
				array(
					'name'  => esc_html__( 'White', 'stwp' ),
					'slug'  => 'white',
					'color' => $white,
				),
			)
		);

		add_theme_support(
			'editor-gradient-presets',
			array(
				array(
					'name'     => esc_html__( 'Dark gray to gray', 'stwp' ),
					'gradient' => 'linear-gradient(60deg, ' . $dark_gray . ' 0%, ' . $gray . ' 100%)',
					'slug'     => 'dark-gray-to-gray',
				),
			)
		);
	}
	add_action( 'after_setup_theme', 'stwp_theme_setup' );
}
