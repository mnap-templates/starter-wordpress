<?php
/**
 * Functions and definitions
 *
 * This function dequeues and deregisteres parent theme stylesheets
 * and should be placed in the `functions.php` file of a theme or the main file
 * of a plugin.
 *
 * It takes an array of a parent theme stylesheet handles, dequeues and
 * deregisteres them.
 * NOTE: Block editor stylesheets should be dequeued in a diffrent action hook.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package STWP
 */

/**
 * Dequeue and deregister parent styles.
 *
 * @return void
 */
function stwp_dequeue_styles() {
	$parent_styles = array(
		'parent-theme-style',
		'parent-theme-print-style',
		'parent-theme-other-style',
	);
	foreach ( $parent_styles as $style ) {
		wp_dequeue_style( $style );
		wp_deregister_style( $style );
	}
}
add_action( 'wp_print_styles', 'stwp_dequeue_styles' );

/**
 * Dequeue and deregister editor parent styles.
 *
 * @return void
 */
function stwp_dequeue_editor_styles() {
	$parent_editor_styles = array(
		'parent-theme-editor-styles',
		'parent-theme-custom-color-overrides',
	);
	foreach ( $parent_editor_styles as $style ) {
		wp_dequeue_style( $style );
		wp_deregister_style( $style );
	}
}
add_action( 'enqueue_block_assets', 'stwp_dequeue_editor_styles' );
