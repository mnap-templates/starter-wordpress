<?php
/**
 * Sets MU Plugin ready for translation. Adds translated strings files in the
 * `languages` directory.
 *
 * @package STWP
 */

/**
 * Load mu-plugin text domain.
 */
function stwp_load_block_patterns_textdomain() {
	load_muplugin_textdomain( 'stwp-muplugin-textdomain', 'languages' );
}
add_action( 'muplugins_loaded', 'stwp_load_muplugin_textdomain' );
