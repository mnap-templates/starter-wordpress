<?php
/**
 * Block Styles
 *
 * NOTE: Only name and label are rewuired to register custom block style.
 * It will create an `is-style-name` class for a block (in this case
 * `is-style-stwp-style-name`) which can be used in any stylesheet.
 * If you want to add separate stylesheet you have to register it first and add
 * `style_handle` to the array.
 *
 * @package STWP
 */

if ( function_exists( 'register_block_style' ) ) {
	/**
	 * Add custom block styles.
	 */
	function stwp_register_block_styles() {
		/**
		 * Register stylesheet.
		 */
		if ( defined( 'WP_DEBUG' ) && true === WP_DEBUG ) {
			$min = '';
		} else {
			$min = '.min';
		}

		$style_handle = 'stwp-block-style';
		// TODO: remove this comment in prduction. $plugin_data = get_file_data( plugin_dir_path( __DIR__ ) . 'plugin-name.php', array( 'Version' => 'Version' ) ); // Requires version metadata in a plugin header.
		// TODO: remove this comment in prduction. $plugin_version = $plugin_data[ 'Version' ]; // Requires version metadata in a plugin header.
		$plugin_version = filemtime( plugin_dir_path( __FILE__ ) . 'assets/css/plugin-style.css' ); // Volatile versioning for a development.

		wp_register_style(
			$style_handle,
			plugins_url( 'assets/css/plugin-style' . $min . '.css', __FILE__ ),
			array(),
			$plugin_version,
			'all'
		);

		// RTL styles.
		wp_style_add_data( $style, 'rtl', 'replace' );
		wp_style_add_data( $style, 'suffix', $min );

		/**
		 * Register block style.
		 */
		register_block_style(
			'core/block',
			array(
				'name'         => 'stwp-style-name',
				'label'        => esc_html__( 'Style name', 'stwp' ),
				'style_handle' => 'stwp-block-style',
			)
		);
	}
	add_action( 'init', 'stwp_register_block_styles' );
}

/**
 * Load mu-plugin text domain.
 */
function stwp_load_block_styles_textdomain() {
	load_muplugin_textdomain( 'stwp', 'languages' );
}
add_action( 'muplugins_loaded', 'stwp_load_block_styles_textdomain' );
