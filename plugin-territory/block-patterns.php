<?php
/**
 * Block patterns.
 *
 * @package STWP
 */

/**
 * Register block pattetn category.
 */
if ( function_exists( 'register_block_pattern_category' ) ) {
	register_block_pattern_category(
		'stwp',
		array( 'label' => esc_html__( 'STWP', 'stwp' ) )
	);
}

if ( function_exists( 'register_block_pattern' ) ) {
	/**
	 * Register block patterns.
	 */
	function stwp_register_block_patterns() {
		register_block_pattern(
			'stwp/block-pattern',
			array(
				'title'         => esc_html__( 'Block Pattern', 'stwp' ),
				'categories'    => array( 'stwp' ),
				'viewportWidth' => 1280,
				'content'       => '<!-- wp:block --><div class="example-block-pattern">' . esc_html__( 'Insert text...', 'stwp' ) . '<\div><!-- /wp:block -->',
			)
		);
	}
	add_action( 'init', 'stwp_register_block_patterns' );
}

/**
 * Load mu-plugin text domain.
 */
function stwp_load_block_patterns_textdomain() {
	load_muplugin_textdomain( 'stwp', 'languages' );
}
add_action( 'muplugins_loaded', 'stwp_load_block_patterns_textdomain' );
