# Plugin Territory

This directory contains files and functions that should be used within a plugin only.

## Files

- `custom-library-loader.php` - intended as a mu-plugins loader for themes; helps to load plugins from subdirectories 
- `index.php` - prevents misconfigured servers from directory listing; it can be used in a plugins, mu-plugins, includes and templates directories

