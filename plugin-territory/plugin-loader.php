<?php
/**
 * Example Plugin Loader
 *
 * @package           STWP
 * @author            mnap
 * @copyright         2021 mnap
 * @license           GPL-2.0-or-later
 *
 * @link https://bbpress.trac.wordpress.org/browser/trunk/bbpress.php
 *
 * @wordpress-plugin
 * Plugin Name:       Example Plugin Loader
 * Plugin URI:        https://gitlab.com/mnap-templates/starter-wordpress
 * Description:       Loads proper plugin file which may be separate file, includes or class.
 * Version:           1.0.0
 * Requires at least: 5.3
 * Requires PHP:      7.2
 * Author:            mnap
 * Author URI:        https://gitlab.com/mnap
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
 * Text Domain:       stwp-custom-library-loader
 * Domain Path:       /languages
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

// Assume you want to load from build.
$stwp_plugin_loader = __DIR__ . '/class-plugin-loader.php'; // phpcs:ignore PHPCompatibility.Keywords.NewKeywords.t_dirFound

// Include plugin file.
require $stwp_plugin_loader; // phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound

// Unset the loader, since it's loaded in global scope.
unset( $stwp_plugin_loader );
