<?php
/**
 * The base configuration for WordPress - staging version
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file is for staging server and contains the
 * following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * Debugging
 * * Error logging
 * * Memory limit
 * * Dashboard file editor
 * * SSL logins
 * * unfiltered HTML
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'db_name' );

/** MySQL database username */
define( 'DB_USER', 'db_user_name' );

/** MySQL database password */
define( 'DB_PASSWORD', 'db_password' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '');
define('SECURE_AUTH_KEY',  '');
define('LOGGED_IN_KEY',    '');
define('NONCE_KEY',        '');
define('AUTH_SALT',        '');
define('SECURE_AUTH_SALT', '');
define('LOGGED_IN_SALT',   '');
define('NONCE_SALT',       '');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );

/**
 * This will log all errors notices and warnings to a /wp-content/debug.log
 * file only when WP_DEBUG is true.
 * You may want also display errors to the screen (now disabled).
 * If Apache does not have write permission, you may need to create the file
 * first and set the appropriate permissions (i.e. use 660).
 */
if ( WP_DEBUG ) {
	define( 'WP_DEBUG_LOG', true );
	define( 'WP_DEBUG_DISPLAY', false );
}

/**
 * For developers: PHP error logging.
 *
 * Staging server's PHP configuration overrides. They should follow your
 * WP_DEBUG settings.
 * Ideally, this should be set in php.ini, if you have access. Error log path
 * should be outside a publicly accesible location or acces should be disabled
 * via .htaccess or by setting file permission.
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php#Configure_Error_Logging
 */
@ini_set( 'log_errors', 'On' );
@ini_set( 'display_errors', 'Off' );
// @ini_set( 'error_log', '/home/example.com/logs/php_error.log' );
// Uncomment and set appriopriate path.

/**
 * Increasing memory allocated to PHP.
 *
 * This setting may be necessary in the event you receive a message such as
 * "Allowed memory size of xxxxxx bytes exhausted". This setting may not work
 * if your host does not allow for increasing the PHP memory limit.
 * When using WooCommerce plugin both limits should be raised to 256M.
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php#Increasing_memory_allocated_to_PHP
 * @link https://docs.woocommerce.com/document/increasing-the-wordpress-memory-limit/
 */
define( 'WP_MEMORY_LIMIT', '64M' );
define( 'WP_MAX_MEMORY_LIMIT', '256M' );

/**
 * Disabling the plugin and theme editor in dashboard.
 *
 * Prevents overzealous users from being able to edit sensitive files and
 * potentially crash the site.
 * Please note: the functionality of some plugins may be affected by the use of
 * current_user_can('edit_plugins') in their code.
 */
define( 'DISALLOW_FILE_EDIT', true );

/**
 * Requiring SSL for admin and logins.
 *
 * Secures logins and the admin area so that both passwords and cookies are
 * never sent in the clear.
 */
define( 'FORCE_SSL_ADMIN', true );

/**
 * Disable unfiltered HTML for all users, including administrators. By default
 * Users with Administrator or Editor roles are allowed to publish unfiltered
 * HTML in post titles, post content, and comments.
 */
define( 'DISALLOW_UNFILTERED_HTML', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
