<?php
/**
 * Custom template tags for this theme.
 *
 * @package STWP
 */

/**
 * Prints post thumbnail as a background style.
 */
function stwp_header_background_image() {
	printf(
		'style="background-image: url(%s)"',
		esc_url( get_the_post_thumbnail_url() )
	);
}
