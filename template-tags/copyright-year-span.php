<?php
/**
 * Custom template tags for a theme.
 *
 * @package STWP
 */

/**
 * Prints copyright sign with dynamically created year span.
 */
function stwp_copyright_date_span() {
	$initial_year = '2021';
	$current_year = gmdate( 'Y' );
	$year_span    = '';
	if ( $initial_year !== $current_year ) {
		$year_span = $initial_year . '-' . $current_year;
	} else {
		$year_span = $current_year;
	}
	printf(
		/* translators: %s Year span. */
		esc_html__( '&copy; %s ', 'stwp' ),
		$year_span // phpcs:ignore WordPress.Security.EscapeOutput
	);
}

