# Simple WordPress Starter

## Description

This is simple WordPress starter meant for using with XAMPP local server. It is meant to work on Linux and Windows, but might work on OS X/MacOS as well.
NPM scripts and project's directory structure adapted to work well with Underscores starter theme.

## Prerequisites

* Install PHP Code Sniffer (phpcs) and appropriate rule sets of WordPress Coding Standard. Any coding standard that fits you should do.
* Set XAMPP_VHOST_SLUG variable in your environment (e.g. `~/.profile`), which points to our virtual host public directory (e.g. `public_html` or `www`). SLUG part might be your project's name.
* Set PROXY_SLUG variable as described above, which is virtual host's local address.
* Adjust theme and (mu-)plugins directories location in the config section.
* Adjust assets location, i.e. css, js, images and (mu-)plugins directories location in the scripts section.
* Delete or adjust other scripts.
