<?php
/**
 * Cleanup WordPress header area.
 *
 * @link https://webdesignviews.com/clean-up-wordpress-header/
 *
 * @package STWP
 */

if ( ! function_exists( 'stwp_header_cleanup' ) ) :
	/**
	 * Removes some not necessary elements from websites header.
	 *
	 * This function have to be adjusted to specific page needs. It may remove
	 * some meta and link tags which are necessary for some plugins and apps
	 * to work with that page.
	 */
	function stwp_header_cleanup() {

		/**
		 * Removes WordPress version number from head file and RSS feeds.
		 *
		 * @link https://www.wpbeginner.com/wp-tutorials/the-right-way-to-remove-wordpress-version-number/
		 */
		function stwp_remove_wp_version() {
			return '';
		}
		add_filter( 'the_generator', 'stwp_remove_wp_version' );

		/**
		 * Removes query strings with versions from all static resources.
		 *
		 * Probably it is better to use $ver=false in enqueuing scripts and
		 * stylesheets.
		 * Anyway, version helps to resolve caching problems but default setting
		 * leaks WordPress version if there is no version of a script or
		 * stylesheet set.
		 *
		 * @link https://crunchify.com/how-to-clean-up-wordpress-header-section-without-any-plugin/
		 * @param string $src The script's or stylesheet's source URL.
		 */
		function stwp_cleanup_query_string( $src ) {
			$parts = explode( '?', $src );
			return $parts[0];
		}
		add_filter( 'script_loader_src', 'stwp_cleanup_query_string' );
		add_filter( 'style_loader_src', 'stwp_cleanup_query_string' );

		/**
		 * Removes feed links and comments feed.
		 */
		remove_action( 'wp_head', 'feed_links', 2 ); // Feed links.
		remove_action( 'wp_head', 'feed_links_extra', 3 ); // Comments feed.

		/**
		 * Removes emoji scripts and styles.
		 *
		 * @link https://www.wpfaster.org/code/how-to-remove-emoji-styles-scripts-wordpress
		 */
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		remove_action( 'admin_print_styles', 'print_emoji_styles' );

		/**
		 * Removes post relational links.
		 *
		 * @link https://www.isitwp.com/remove-code-wordpress-header/
		 */
		remove_action( 'wp_head', 'index_rel_link' );
		remove_action( 'wp_head', 'start_post_rel_link' );
		remove_action( 'wp_head', 'parent_post_rel_link' );
		remove_action( 'wp_head', 'adjacent_posts_rel_link' );
		remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head' );

		/**
		 * Removes api.w.org relation link.
		 *
		 * @link https://crunchify.com/how-to-clean-up-wordpress-header-section-without-any-plugin/
		 */
		remove_action( 'wp_head', 'rest_output_link_wp_head' );
		remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
		remove_action( 'wp_head', 'wp_oembed_add_host_js' );
		remove_action( 'template_redirect', 'rest_output_link_header' );

		/**
		 * Removes XML-RPC RSD, Windows Live Writer and shortlink links.
		 *
		 * @link https://www.isitwp.com/remove-code-wordpress-header/
		 */
		remove_action( 'wp_head', 'rsd_link' );
		remove_action( 'wp_head', 'wlwmanifest_link' );

		/**
		 * Removes dns-prefetch and shortlinks.
		 *
		 * @link https://fransdejonge.com/2017/11/remove-dns-prefetch-from-wordpress-4-9/
		 */
		remove_action( 'wp_head', 'wp_shortlink_wp_head' );
		remove_action( 'wp_head', 'wp_resource_hints', 2 ); // May interfere with Jetpack.
	};
endif;
add_action( 'after_setup_theme', 'stwp_header_cleanup' );
