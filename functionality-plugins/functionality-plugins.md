# Functionality Plugins

This directory contains files that can be used as a plugins or must-use plugins. They can be put directly to a `plugins` or `mu-pluins` folder or loaded from a separate file (see `plugin-territory` folder for an example `custom-loader`).

# References

Read through links below for some examples which files in this folder are based on:
https://make.wordpress.org/themes/handbook/review/how-to-do-a-review-draft/#plugin-territory-and-non-design-related-functionality
https://www.billerickson.net/core-functionality-plugin/
https://github.com/billerickson/ea-core-functionality
https://css-tricks.com/wordpress-functionality-plugins/
https://github.com/chriscoyier/css-tricks-functionality-plugin
