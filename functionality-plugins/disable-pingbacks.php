<?php
/**
 * Disable the pingack functionality of XML-RPC which prevents your site from
 * participating in pingback DoS attacks.
 *
 * @link https://wptavern.com/how-to-prevent-wordpress-from-participating-in-pingback-denial-of-service-attacks
 * @link https://marketingwithvladimir.com/3-methods-disable-xmlrpc-pingback-wordpress/
 * @link https://nfsec.pl/security/6296 [PL]
 * @package STWP
 */

/**
 * Removes pingback.ping method of XML-RPC.
 *
 * @param string[] $methods An array of XML-RPC methods, keyed by their methodName.
 */
function stwp_remove_xmlrpc_pingback_ping( $methods ) {
	unset( $methods['pingback.ping'] );
	return $methods;
};
add_filter( 'xmlrpc_methods', 'stwp_remove_xmlrpc_pingback_ping' );

/**
 * Removes X-Pingback HTTP header.
 *
 * @param string[] $headers Associative array of headers to be sent.
 */
function stwp_remove_x_pingback_http_header( $headers ) {
	unset( $headers['X-Pingback'] );
	return $headers;
};
add_filter( 'wp_headers', 'stwp_remove_x_pingback_http_header' );
