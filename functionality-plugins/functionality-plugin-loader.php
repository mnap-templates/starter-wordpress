<?php
/**
 * Functionality Plugin Loader
 *
 * @link https://github.com/WordPress/wporg-mu-plugins/blob/trunk/README.md
 * @link https://developer.wordpress.org/reference/functions/plugin_dir_path/
 * @link https://github.com/billerickson/ea-core-functionality
 *
 * @package           STWP
 * @author            mnap
 * @copyright         2021 mnap
 * @license           GPL-2.0-or-later
 *
 * @wordpress-plugin
 * Plugin Name:       Functionality Plugin Loader
 * Plugin URI:        https://gitlab.com/mnap-templates/starter-wordpress
 * Description:       Loads custom plugins for the project. Intended as a mu-plugins loader for themes.
 * Version:           1.0.0
 * Requires at least: 5.3
 * Requires PHP:      7.2
 * Author:            mnap
 * Author URI:        https://gitlab.com/mnap
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
 * Text Domain:       stwp-custom-library-loader
 * Domain Path:       /languages
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

define( 'STWP_MUPLUGINS_DIR', plugin_dir_path( __FILE__ ) );

require_once STWP_MUPLUGINS_DIR . 'custom-plugin-one/custom-plugin-one.php'; // phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound
require_once STWP_MUPLUGINS_DIR . 'custom-plugin-two.php'; // phpcs:ignore WPThemeReview.CoreFunctionality.FileInclude.FileIncludeFound

