<?php
/**
 * Nav Menu additional CSS classes for a custop post type archive page,
 * Posts Page and single post or custom post.
 *
 * @link https://developer.wordpress.org/reference/hooks/nav_menu_css_class/
 *
 * @package STWP
 */

/**
 * Adds current-menu-ancestor and current-menu-parent classes to a custom post
 * type archive's menu item and Posts Page menu item. It allows automatic menu
 * item highlighting for post archives, Posts Page and single posts.
 *
 * WARNING: this function depends on menu item's url and permalink comparison.
 * If they differ even slightly (eg. http:// vs. https://) it will not work.
 * Do not use a menu's 'Custom Links' but an 'All <Custom Post Type>' menu item.
 * It can be activated in 'Screen Options -> Boxes -> <Custom Post Type>'
 * settings menu at the top of the admin's screen.
 *
 * @param array  $classes Nav menu item classes.
 * @param object $item    Nav menu item data object.
 * @param array  $args    Nav menu arguments.
 * @param int    $depth   Depth of menu item. Used for padding.
 */
function stwp_add_menu_css_classes( $classes, $item, $args, $depth ) {
	// Create post type, url and permalink variables.
	$item_url  = $item->url;
	$post_type = get_post_type();
	$permalink = get_post_type_archive_link( $post_type );

	if ( $item_url === $permalink && ! is_search() ) {
		// Add current-menu-item class if not present.
		if ( ! in_array( 'current-menu-item', $classes, true ) ) {
			$classes[] = 'current-menu-item';
		}
		// Add current-menu-parent and -ancestor classes.
		$classes[] = 'current-menu-parent current-menu-ancestor';
	}
	return $classes;
}
add_filter( 'nav_menu_css_class', 'stwp_add_menu_css_classes', 10, 4 );

/**
 * Adds aria-current attribute to link which is a child of a current-menu-item.
 * Improves accesibility.
 *
 * @param array  $atts  HTML attributes applied to the menu item's <a> element.
 * @param object $item  The current menu item.
 * @param array  $args  Menu item's arguments.
 * @param int    $depth Depth of menu item. Used for padding.
 */
function stwp_add_menu_attributes( $atts, $item, $args, $depth ) {
	// Create post type, url and permalink variables.
	$item_url  = $item->url;
	$post_type = get_post_type();
	$permalink = get_post_type_archive_link( $post_type );

	if ( $item_url === $permalink && ! is_search() ) {
		$atts['aria-current'] = 'page';
	}
	return $atts;
}
add_filter( 'nav_menu_link_attributes', 'stwp_add_menu_attributes', 10, 4 );

/**
 * Removes current_page_parent class from Posts Page menu item when viewing
 * custom post type's page or archive.
 * It exists in WordPress for back compatibility reasons and is scheduled to
 * fix. See link below.
 *
 * @link https://core.trac.wordpress.org/ticket/38486
 *
 * @param array  $classes Nav menu item classes.
 * @param object $item    Nav menu item data object.
 * @param array  $args    Nav menu arguments.
 * @param int    $depth   Depth of menu item. Used for padding.
 */
function stwp_remove_parent_menu_css_class( $classes, $item, $args, $depth ) {
	// Create array of all public custom post types.
	$args     = array(
		'public'   => true,
		'_builtin' => false,
	);
	$output   = 'names';
	$operator = 'and';

	$custom_post_types = get_post_types( $args, $output, $operator );

	// Remove class from blog's menu item when on custom post type's page
	// or archive.
	if ( is_post_type_archive( $custom_post_types ) ||
		is_singular( $custom_post_types ) &&
		get_post_meta( $item->ID, '_menu_item_object_id', true ) ===
		get_option( 'page_for_posts' )
	) {
		$classes = array_diff( $classes, array( 'current_page_parent' ) );
	}
	return $classes;
}
add_filter( 'nav_menu_css_class', 'stwp_remove_parent_menu_css_class', 10, 4 );
