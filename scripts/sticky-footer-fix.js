/**
 * File sticky-footer-fix.js.
 *
 * Moves footer to the bottom of the page. Prevents footer from floating in
 * the middle of the screen when the content is to short to push it to
 * the bottom.
 *
 * Snippet taken from: https://stackoverflow.com/questions/643879/css-to-make-html-page-footer-stay-at-bottom-of-the-page-with-a-minimum-height-b#46068757
 * Rewritten in pure JavaScript.
 */
( function () {
	const page = document.getElementById( 'page' );

	if ( page.clientHeight < window.innerHeight ) {
		const content = document.getElementById( 'content' );
		const header = document.getElementById( 'masthead' );
		const footer = document.getElementById( 'colophon' );

		const setContentMinHeight = function () {
			content.style.minHeight =
				window.innerHeight -
				header.clientHeight -
				footer.clientHeight +
				'px';
		};

		window.addEventListener( 'resize', setContentMinHeight() );
	}
} )();
