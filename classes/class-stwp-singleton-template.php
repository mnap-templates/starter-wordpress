<?php
/**
 * STWP Singleton Template
 *
 * @package          STWP
 * @author           mnap
 * @copyright        2021 mnap
 * @license          GPL-2.0-or-later
 *
 * @wordpress-plugin
 * Plugin Name:       STWP Singleton Template
 * Plugin URI:        https://gitlab.com/mnap-templates/starter-wordpress
 * Description:       A plugin with singleton class template.
 * Version:           1.0.0
 * Requires at least: 5.3
 * Requires PHP:      7.2
 * Author:            mnap
 * Author URI:        https://gitlab.com/mnap
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
 * Text Domain:       stwp-singleton-template
 * Domain Path:       /languages
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'STWP_Singleton_Template' ) ) {

	/**
	 * The core plugin class.
	 */
	class STWP_Singleton_Template {

		/**
		 * Plugin version to avoid caching issues.
		 * Start at version 1.0.0 and use SemVer - https://semver.org
		 *
		 * @var string $version Plugin version number.
		 */
		private $version = '1.0.0';

		/**
		 * Static property to hold singleton instance.
		 *
		 * @var string|null $instance Holds instance property.
		 */
		private static $instance = null;

		/**
		 * Make class constructor private so nobody can call `new Class`.
		 */
		private function __construct() {}

		/**
		 * Make clone magic method private, so nobody can clone instance.
		 */
		private function __clone() {}

		/**
		 * Make sleep magic method private, so nobody can serialize instance.
		 */
		private function __sleep() {} // phpcs:ignore PHPCompatibility.FunctionDeclarations.NonStaticMagicMethods.__sleepMethodVisibility

		/**
		 * Make wakeup magic method private, so nobody can unserialize instance.
		 */
		private function __wakeup() {}

		/**
		 * If an instance exists, this returns it. If not, it creates one and
		 * returns it.
		 *
		 * @return STWP_Singleton_Template
		 */
		public static function get_instance() {
			if ( null === self::$instance ) {
				self::$instance = new self();
				self::$instance->init();
			}
			return self::$instance;
		}

		/**
		 * Initialization hooks.
		 */
		public static function init() {

			// Back-end.
			add_action( 'init', array( __CLASS__, 'backend_example_function' ), 10 );

			// Front-end.
			add_action( 'init', array( __CLASS__, 'frontend_example_function' ), 10 );
		}

		/**
		 * Back-end dummy function.
		 */
		public static function backend_example_function() {
			// ...
		}

		/**
		 * Front-end dummy function.
		 */
		public static function frontend_example_function() {
			// ...
		}
	}
}

/**
 * Returns only one instance of the plugin.
 *
 * This example initialization follows bbPress plugin. This function could be
 * called from within action hook or in a separate loader file.
 *
 * @see https://bbpress.trac.wordpress.org/browser/trunk/src/bbpress.php
 * @see https://developer.woocommerce.com/extension-developer-guide/designing-a-simple-extension/
 *
 * @return STWP_Singleton_Template The only instance of the plugin.
 */
function stwp_singleton_template() {
	return STWP_Singleton_Template::get_instance();
}

stwp_singleton_template();
