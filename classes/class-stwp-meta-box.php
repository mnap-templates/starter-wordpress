<?php
/**
 * This is an example of how to add a meta box from inside a class.
 *
 * @link https://developer.wordpress.org/reference/functions/add_meta_box/
 * @link https://wordpress.stackexchange.com/questions/142004/post-php-conditional-statements-for-new-post-and-edit-post
 *
 * @package STWP
 */

/**
 * Calls the class on the post edit screen.
 */
function stwp_meta_box_instance() {
	new STWP_Meta_Box();
}

if ( is_admin() ) {
	// Add meta box when EDITING post.
	add_action( 'load-post.php', 'stwp_meta_box_instance' );
	// Add meta box when creating NEW post.
	add_action( 'load-post-new.php', 'stwp_meta_box_instance' );
}

/**
 * The Class.
 */
class STWP_Meta_Box {

	/**
	 * Hook into the appropriate actions when the class is constructed.
	 */
	public function __construct() {
		add_action( 'add_meta_boxes', array( $this, 'register_meta_box' ), 10, 2 );
		add_action( 'save_post', array( $this, 'save' ), 10, 2 );
	}

	/**
	 * Registers meta box.
	 *
	 * @param string  $post_type Post type.
	 * @param WP_Post $post Post object.
	 */
	public function register_meta_box( string $post_type, WP_Post $post ) {

		// Limit meta box to certain post types.
		$post_types = array( 'post', 'page' );

		if ( in_array( $post_type, $post_types, true ) ) {
			add_meta_box(
				'meta_box_name',
				__( 'Meta Box Title', 'stwp' ),
				array( $this, 'render_meta_box' ),
				$post_type,
				'advanced',
				'high'
			);
		}
	}

	/**
	 * Save the meta when the post is saved.
	 *
	 * @param int     $post_id Post ID.
	 * @param WP_Post $post    Post object.
	 */
	public function save( int $post_id, WP_Post $post ) {

		// Checks save status.
		$is_autosave = wp_is_post_autosave( $post_id );
		$is_revision = wp_is_post_revision( $post_id );
		// Check nonce.
		$is_valid_nonce = ( isset( $_POST['meta_box_nonce'] ) && wp_verify_nonce( sanitize_key( $_POST['meta_box_nonce'] ), basename( __FILE__ ) ) ) ? 'true' : 'false';

		// Exits script depending on save status and nonce value.
		if ( $is_autosave || $is_revision || ! $is_valid_nonce ) {
			return $post_id;
		}

		// Check if there was a multisite switch before.
		if ( is_multisite() && ms_is_switched() ) {
			return $post_id;
		}

		// Check user permissions.
		$is_page = ( isset( $_POST['post_type'] ) && 'page' === $_POST['post_type'] ) ? 'true' : 'false';
		if ( $is_page && ( ! current_user_can( 'edit_page', $post_id ) ) || ( ! current_user_can( 'edit_post', $post_id ) ) ) {
			return $post_id;
		}

		/* OK, it's safe for us to save the data now. */

		// Save meta. Check if fields are present (i.e. do not save on restore).
		if ( isset( $_POST['_meta_box_text'] ) ) {
			$meta  = get_post_meta( $post_id, '_meta_box_text', true );
			$input = sanitize_text_field( wp_unslash( $_POST['_meta_box_text'] ) );

			if ( $input && '' === $meta ) {
				add_post_meta( $post_id, '_meta_box_text', $input, true );
			} elseif ( $input && $input !== $meta ) {
				update_post_meta( $post_id, '_meta_box_text', $input );
			} elseif ( '' === $input && $meta ) {
				delete_post_meta( $post_id, '_meta_box_text', $meta );
			}
		}
	}

	/**
	 * Render Meta Box content.
	 *
	 * @param WP_Post $post The post object.
	 */
	public function render_meta_box( WP_Post $post ) {

		// Add an nonce field so we can check for it later.
		wp_nonce_field( basename( __FILE__ ), 'meta_box_nonce' );

		// Use get_post_meta to retrieve an existing value from the database.
		$value = get_post_meta( $post->ID, '_meta_box_text', true );

		// Display the form, using the current value.
		?>
		<label for="meta-box-text">
			<?php esc_html_e( 'Description for this field', 'stwp' ); ?>
		</label>
		<input type="text" id="meta-box-text" name="_meta_box_text" value="<?php echo esc_attr( $value ); ?>" size="25" />
		<?php
	}
}
