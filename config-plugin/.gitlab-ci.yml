image: node:14-bullseye

variables:
  # Project's variables.
  BUILD_DIR: "."
  # Deployment directories. Use relative paths.
  DEPLOY_DIR: "/wp-content/mu-plugins"
  STAGE_PUBLIC_DIR: "public_html"
  PROD_PUBLIC_DIR: "public_html"
  # Protected variables that should be set in GitLab's CI/CD settings.
  STAGE_URL: "$STAGE_URL"
  STAGE_FTP_URL: "$STAGE_FTP_URL"
  STAGE_FTP_USER: "$STAGE_FTP_USER"
  STAGE_FTP_PASSWD: "$STAGE_FTP_PASSWD"
  PROD_URL: "$PROD_URL"
  PROD_FTP_URL: "$PROD_FTP_URL"
  PROD_FTP_USER: "$PROD_FTP_USER"
  PROD_FTP_PASSWD: "$PROD_FTP_PASSWD"

cache:
  # Set cache per project. Project"s ID should be most reliable as a key.
  key: "${CI_PROJECT_ID}"
  paths:
    # Cache NodeJS modules and PHP libraries in between jobs.
    - node_modules
    - vendor

stages:
  - build
  - deploy

before_script:
  - apt update -qq && apt install php-pear -y -qq
  # Install PHP_CodeSniffer; PEAR method used.
  - pear install PHP_CodeSniffer
  # Adjust rulesets and git branches to the project.
  - git clone -b master https://github.com/WordPress/WordPress-Coding-Standards.git /tmp/wpcs
  - git clone -b master https://github.com/PHPCompatibility/PHPCompatibility.git /tmp/phpc
  - git clone -b master https://github.com/PHPCompatibility/PHPCompatibilityWP.git /tmp/phpcwp
  - git clone -b master https://github.com/PHPCompatibility/PHPCompatibilityParagonie.git /tmp/phpcp
  - git clone -b master https://github.com/WPTRT/WPThemeReview.git /tmp/wptr
  - phpcs --config-set installed_paths /tmp/wpcs,/tmp/phpc,/tmp/phpcwp,/tmp/phpcp,/tmp/wptr
  # Install NodeJS modules.
  - npm install
  # Dummy dotenv, otherwise some npm scripts will fail.
  - touch .env

build_project:
  stage: build
  script:
    - echo "Build project"
    - npm run build
  rules:
    - if: '$CI_COMMIT_BRANCH == "dev"'
      when: always
    - if: '$CI_COMMIT_TAG'
      when: always
  retry:
    max: 2
    when:
      - runner_system_failure
      - stuck_or_timeout_failure
  artifacts:
    paths:
      - '**.js'
      - '**.css'
      - '**.map'
    exclude:
      - 'src/**'

.deploy_staging:
  stage: deploy
  before_script:
    - apt update -qq && apt install lftp -y -qq
  script:
    - echo "Deploy to staging server"
    - lftp -e
      "set ftp:ssl-force yes;
      set ssl:verify-certificate no;
      open $STAGE_FTP_URL;
      user $STAGE_FTP_USER $STAGE_FTP_PASSWD;
      mkdir -p ${STAGE_PUBLIC_DIR}${DEPLOY_DIR};
      mirror --continue --delete --reverse --verbose
      --exclude='src/' --exclude='node_modules/'
      --exclude='README.md'
      --exclude-glob='package*.json' --exclude-glob='.*' --exclude-glob='.*/'
      ${BUILD_DIR}
      ${STAGE_PUBLIC_DIR}${DEPLOY_DIR};
      bye"
  rules:
    - if: '$CI_COMMIT_BRANCH == "dev"'
      when: on_success
    - if: '$CI_COMMIT_TAG'
      when: on_success
  environment:
    name: staging
    url: "$STAGE_URL"
  retry:
    max: 2
    when:
      - runner_system_failure
      - stuck_or_timeout_failure
  dependencies:
    - build_project

.deploy_prod:
  stage: deploy
  before_script:
    - apt update -qq && apt install lftp -y -qq
  script:
    - echo "Deploy to production server"
    - lftp -e
      "set ftp:ssl-force yes;
      set ssl:verify-certificate no;
      open $PROD_FTP_URL;
      user $PROD_FTP_USER $PROD_FTP_PASSWD;
      mkdir -p ${PROD_PUBLIC_DIR}${DEPLOY_DIR};
      mirror --continue --delete --reverse --verbose
      --exclude='src/' --exclude='node_modules/'
      --exclude='README.md'
      --exclude-glob='package*.json' --exclude-glob='.*' --exclude-glob='.*/'
      ${BUILD_DIR}
      ${PROD_PUBLIC_DIR}${DEPLOY_DIR};
      bye"
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+(-\w*)?$/'
      when: manual
  environment:
    name: production
    url: "$PROD_URL"
  retry:
    max: 2
    when:
      - runner_system_failure
      - stuck_or_timeout_failure
  dependencies:
    - build_project
